const maincta = () => `
<section class="mainCta container">
    <div class="section-info">
    <h3 class="section-subtitle">
    Subscribe
    </h3>
    <div class="section-text">
        Lorem ipsum dolor sit amet, consectetur
    </div>
    </div>

    <div class="cta-button">
    <input type="email"
        name="email"
        placeholder="Enter Your email address"
    />
    <button class="btn button-red">Subscribe</button>
    </div>
    </section>`;

export default maincta;

