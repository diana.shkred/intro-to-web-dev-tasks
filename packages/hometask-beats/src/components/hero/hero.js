const hero = () => `
<section class="hero-section container ">
          <div class="hero-img"></div>
          <div class="hero-info">
            <h3 class="hero-subtitle section-subtitle">
            Hear it. Feel it
            </h3>
            <h1 class="hero-title">
            Move with the music
            </h1>
            <div class="hero-price">
              <div class="hero-price-new">$ 435</div>
              <div class="hero-price-old">$ 465</div>
            </div>
            <button class="btn button-white">
            BUY NOW
            </button>
          </div> 
        </section>`;

export default hero;

