import header from "../components/header/header.js";
import hero from "../components/hero/hero.js";

const headerContainer = () => `
    <div class="wrapper-red">
        ${header()}
        ${hero()}
    </div>
`;

export default headerContainer;

