const body = document.querySelector("body");
const wrapper = document.createElement("div");
wrapper.classList.add("wrapper", "landing");
const header = document.createElement("header");
const main = document.createElement("main");
const footer = document.createElement("footer");

const headerList = ["Discover", "Join", "Sign in"];

header.innerHTML = `
<div class="header-logo">
  <a href="#">
    <img class="header-logo-img" src="./assets/icons/logo.svg" alt="logo" />
    <div class="header-logo-text">Simo</div>
  </a>
</div>
<nav>
</nav> `;
footer.innerHTML = `
<nav>
  <ul class="footer-nav">
    <li><a href="#">About As</a></li>
    <li><a href="#">Contact</a></li>
    <li><a href="#">CR Info</a></li>
    <li><a href="#">Twitter</a></li>
    <li><a href="#">Facebook</a></li>
  </ul>
</nav>
`;

body.appendChild(wrapper);
wrapper.appendChild(header);
wrapper.appendChild(main);
wrapper.appendChild(footer);
const headernav = document.querySelector("nav");

function provideLandingPage() {
  wrapper.classList.remove("signUp");
  wrapper.classList.add("landing");
  headernav.innerHTML = `
  <ul class="header-nav">
  <li><a class= "discover-link-landing" href="#">${headerList[0]}</a></li>
  <li><a class= "join-link" href="#">${headerList[1]}</a></li>
  <li><a class= "sign-link" href="#">${headerList[2]}</a></li>
</ul>
`;
  main.innerHTML = `
  <section class="section-landing">
  <h1 class="title">Feel the music</h1>

  <div class="landing-text">Stream over 10 million songs with one click</div>

  <button class="button join-link">Join now</button>
</section>
`;
  const joinLink = document.querySelectorAll(".join-link");

  Array.prototype.forEach.call(joinLink, link =>
    link.addEventListener("click", provideSignUpPage)
  );

  const discoverLinkLanding = document.querySelector(
    ".discover-link-landing"
  );
  discoverLinkLanding.addEventListener("click", provideFeaturePage);
}
provideLandingPage();

function provideFeaturePage() {
  wrapper.classList.remove("landing");
  wrapper.classList.remove("signUp");
  headernav.innerHTML = `
<ul class="header-nav">
  <li><a class= "join-link" href="#">${headerList[1]}</a></li>
  <li><a class= "sign-link" href="#">${headerList[2]}</a></li>
</ul> `;
  main.innerHTML = `
  <section class="section-feature">
  <div class="section-feature-left">
    <h1 class="title">Discover new music</h1>
    <div class="section-feature-btns">
      <button class="button">Charts</button>
      <button class="button">Songs</button>
      <button class="button">Artists</button>
    </div>
    <div class="feature-text">
      By joining you can benefit by listening to the latest albums released
    </div>
  </div>
  <div class="feature-img">
    <img src="./assets/images/music-titles.png" alt="feature image" height="512" width="512"
    />
  </div>
</section>`;
  document
    .querySelector(".join-link")
    .addEventListener("click", provideSignUpPage);
}
function provideSignUpPage() {
  headernav.innerHTML = `
  <ul class="header-nav">
  <li><a class= "discover-link-signUp" href="#">${headerList[0]}</a></li>
  <li><a class= "sign-link" href="#">${headerList[2]}</a></li>
</ul> `;
  wrapper.classList.toggle("landing");
  wrapper.classList.toggle("signUp");
  main.innerHTML = `
  <section class="section-signUp">
  <form>  
    <fieldset>  
       <label>Name:</label>
       <input type="text" name="name">
        <label>Password:</label> 
        <input type="current-password" name="password">
        <label>e-mail:</label>
        <input type="email" name="email"> 
      </fieldset>  
 </form>   
  <button class="button">Join now</button>
</section> `;
  document
    .querySelector(".discover-link-signUp")
    .addEventListener("click", provideLandingPage);
}
