const feature = () => `
<section class="feature-section container">
    <div class="feature-info">
    <h3 class="section-subtitle">
        Good headphones and loud music is all you need
    </h3>
    <div class="feature-list">
        <div class="feature-list-item feature-list-1">
        <div class="feature-list-img">
            <div class="feature-list-outer">
            <div class="feature-list-inner">
                <img src="./src/assets/icons/battery.svg" alt="battery"
                width="34"
                height="26"
                />
            </div>
            </div>
        </div>
        <div class="feature-list-info">
            <h4>
            Battery
            </h4>
            <div class="section-text feature-text">
            Battery 6.2V-AAC codec
            </div>
            <a class="feature-list-link" href="#">
            Learn more
            </a>
        </div>
        </div>

        <div class="feature-list-item feature-list-2">
        <div class="feature-list-img">
            <div class="feature-list-outer">
            <div class="feature-list-inner">
                <img src="./src/assets/icons/bluetooth.svg" alt="battery"
                width="34"
                height="26"
                />
            </div>
            </div>
        </div>
        <div class="feature-list-info">
            <h4>
            Bluetooth
            </h4>
            <div class="section-text feature-text">
            Battery 6.2V-AAC codec
            </div>
            <a class="feature-list-link" href="#">
            Learn more
            </a>
        </div>
        </div>

        <div class="feature-list-item feature-list-3">
        <div class="feature-list-img">
            <div class="feature-list-outer">
            <div class="feature-list-inner">
                <img src="./src/assets/icons/microphone.svg" alt="battery"
                width="33"
                height="33"
                />
            </div>
            </div>
        </div>
        <div class="feature-list-info">
            <h4>
            Microphone
            </h4>
            <div class="section-text feature-text">
            Battery 6.2V-AAC codec
            </div>
            <a class="feature-list-link" href="#">
            Learn more
            </a>
        </div>
        </div>
    </div>
    </div>
    <div class="feature-img"></div>
    </section>`;

export default feature;
