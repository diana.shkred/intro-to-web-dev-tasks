const header = () => (`
<header class="header container">
          <div class="logo">
            <a href="#">
            <img src="./src/assets/icons/logo.svg" alt="logo"
                width="73"
                height="66"
            /></a>
          </div>

          <div class="menu mobile-hidden">
            <div class="menu-search">
              <a href="#">
              <img src="./src/assets/icons/search.svg" alt="logo"
                  width="18"
                  height="18"
                  />
                  </a>
            </div>
            <div class="menu-box">
              <a href="#"
                ><img
                  src="./src/assets/icons/box.svg"
                  alt="logo"
                  width="18"
                  height="20"
              /></a>
            </div>
            <div class="menu-user">
              <a href="#"
                ><img
                  src="./src/assets/icons/user.svg"
                  alt="logo"
                  width="16"
                  height="18"
              /></a>
            </div>
          </div>

          <div class="menu-burger">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </header>
`);

export default header;


