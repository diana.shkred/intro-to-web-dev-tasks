const boxing = () => `
<section class="boxing-section container ">
<div class="img"></div>
<div class="boxing-info">
  <h3 class="section-subtitle">
  Whatever you get in the box
  </h3>
  <div class="boxing-list">
    <div class="list">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="arrow-in-circle" />
      5A Charger
    </div>

    <div class="list">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="arrow-in-circle" />
      Extra battery
    </div>

    <div class="list">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="arrow-in-circle" />
      Sophisticated bag
    </div>

    <div class="list">
      <img src="./src/assets/icons/arrow-in-circle.svg" alt="arrow-in-circle" />
      User manual guide
    </div>
  </div>
</div>
</section>`;

export default boxing;

