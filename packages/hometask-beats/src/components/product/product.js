const product = () => `
<section class="product-section container">
    <div class="product-info">
    <h3 class="section-subtitle product-subtitle">
    Our Latest Product
    </h3>
    <div class="section-text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
        facilisis nunc ipsum aliquam, ante.
    </div>
    </div>

    <div class="product-container">
    <div class="product-1 mobile-hidden">
        <div class="product-photo">
        <img src="./src/assets/images/product-01.png" alt="product-01"
            width="227"
            height="306"
        />
        <div class="product-background"></div>
        </div>
        <div class="product-rate">
        <div class="product-rate-stars">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
        </div>
        <div class="product-rate-num">
        4.50
        </div>
        </div>
        <div class="product-selling">
        <div class="product-name">
        Red Headphone
        </div>
        <div class="product-price">
        $ 256
        </div>
        </div>
    </div>

    <div class="product-2">
        <div class="product-photo">
        <img src="./src/assets/images/product-02.png" alt="product-02"
            width="227"
            height="306"
        />
        <div class="product-background"></div>
        </div>
        <div class="product-rate">
        <div class="product-rate-stars">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
        </div>
        <div class="product-rate-num">
        4.50
        </div>
        </div>
        <div class="product-selling">
        <div class="product-name">
        Blue Headphone
        </div>
        <div class="product-price">
        $ 235
        </div>
        </div>
    </div>

    <div class="product-3 mobile-hidden">
        <div class="product-photo">
        <img src="./src/assets/images/product-03.png" alt="product-03"
            width="227"
            height="306"
        />
        <div class="product-background"></div>
        </div>
        <div class="product-rate">
        <div class="product-rate-stars">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
        </div>
        <div class="product-rate-num">
        4.50
        </div>
        </div>
        <div class="product-selling">
        <div class="product-name">
        Green Headphone
        </div>
        <div class="product-price">
        $ 245
        </div>
        </div>
    </div>
    </div>
    </section>`;

export default product;

