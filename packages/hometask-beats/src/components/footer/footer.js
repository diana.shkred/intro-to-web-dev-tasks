const footer = () => `
<footer class="wrapper-red">
    <div class="footer container">
    <div class="logo">
        <a href="#"><img src="./src/assets/icons/logo.svg"
            alt="logo"
            width="73"
            height="66"
        /></a>
    </div>

    <nav class="nav mobile-hidden">
        <ul class="nav-menu">
        <li class="nav-item"><a href="#">Home</a></li>
        <li class="nav-item"><a href="#">About</a></li>
        <li class="nav-item"><a href="#">Product</a></li>
        </ul>
    </nav>

    <div class="menu-socials menu">
        <div class="socials-item">
        <a href="#">
        <img src="./src/assets/icons/instagram.svg" alt="instagram"
            width="20"
            height="20"
        /></a>
        </div>
        <div class="socials-item">
        <a href="#">
            <img src="./src/assets/icons/twitter.svg" alt="twitter"
            width="22"
            height="18"
        /></a>
        </div>
        <div class="socials-item">
        <a href="#">
        <img src="./src/assets/icons/facebook.svg" alt="facebook"
            width="21"
            height="20"
        /></a>
        </div>
    </div>
    </div>
    </footer>`;

export default footer;

